import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import '../imports/client/design/sass/app.scss';

import '../imports/client/routes';
