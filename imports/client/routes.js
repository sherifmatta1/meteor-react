import React from 'react';
import { Router,Route, IndexRoute,browserHistory } from 'react-router';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import MainLayout from './layouts/MainLayout';

// PAGES
import Home from './pages/Home';
import About from './pages/About';

Meteor.startup(() => {

    render(
        <Router history={browserHistory}>
            <Route path="/" component={MainLayout}>
                <IndexRoute component={Home} />
                <Route path="/about" component={About} />
            </Route>
            {/* <Route path="/login" component={Login}/>
            <Route path="/registeration" component={Register}/> */}
        </Router>,
    document.getElementById('render-target')
);
});
