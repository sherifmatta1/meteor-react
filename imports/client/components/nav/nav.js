import React, {Component} from 'react';
import {Meteor} from 'meteor/meteor';
import {Link, browserHistory} from 'react-router';

export default class Nav extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        return (
            <nav className="navbar navbar-light bg-light navbar-expand mb-0">
                <div className="container-fluid">
                    <div className="navbar-brand d-flex">
                        <Link to="/">
                            Brand
                        </Link>
                    </div>
                </div>
            </nav>
        )
    }
}
