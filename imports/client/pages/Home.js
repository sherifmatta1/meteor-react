import React, {Component} from 'react';
import { Link, browserHistory } from 'react-router';
import ReactDOM from 'react-dom';


export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div className="container text-center">
                <h1>Home Page</h1>
                <p>Flexitarian tofu craft beer listicle blog fashion axe post-ironic viral crucifix cloud bread try-hard pop-up glossier typewriter kinfolk. Gastropub VHS echo park, edison bulb master cleanse meh stumptown live-edge sriracha 8-bit food truck. Pitchfork ramps drinking vinegar, beard subway tile food truck flexitarian meh copper mug pabst church-key wolf farm-to-table authentic hammock. Tumeric schlitz woke, scenester four loko unicorn tumblr cornhole celiac cloud bread polaroid. Paleo forage chicharrones, deep v gluten-free tumeric pork belly cloud bread drinking vinegar four loko farm-to-table fixie chia. Sriracha chartreuse tofu intelligentsia. PBR&B green juice sartorial, pitchfork butcher gochujang knausgaard tattooed tumeric copper mug banjo mustache mumblecore. Gentrify organic semiotics coloring book tacos truffaut. Prism squid tattooed chicharrones selvage knausgaard sustainable. Health goth jean shorts schlitz palo santo street art master cleanse tumeric dreamcatcher forage literally gluten-free cloud bread. Air plant flannel health goth four dollar toast intelligentsia sartorial 90's food truck YOLO godard swag deep v chia hot chicken tote bag.</p>
            </div>
        )
    }
};
