import React, {Component} from 'react';

export default class About extends Component {

    render() {
        return (
            <div>
                <h1>About Us</h1>
                <p>
                    Fixie fam fap, affogato wolf gluten-free bespoke keytar viral trust fund before they sold out hammock. Selvage affogato pinterest subway tile. Dreamcatcher locavore ugh chia craft beer man braid. Fingerstache prism fixie, yr pickled pok pok kogi poutine vape letterpress waistcoat. Unicorn readymade chartreuse, helvetica farm-to-table wolf put a bird on it 3 wolf moon master cleanse actually migas flannel vice coloring book cold-pressed. Trust fund +1 mixtape aesthetic. Chillwave flexitarian austin listicle biodiesel vaporware.
                </p>
            </div>
        )
    }
}
