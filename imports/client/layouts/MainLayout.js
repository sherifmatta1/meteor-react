import React from 'react';
import { Link } from 'react-router';
import Nav from '../components/nav/nav';

const MainLayout = ({children}) => {
    return (
        <div>
            <header>
                <Nav />
            </header>
            <main>
                {children}
            </main>
        </div>
    )
}
export default MainLayout;
